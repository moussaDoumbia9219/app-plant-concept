import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import {AppLoading, Asset} from 'expo-asset';

import Navigation from './navigation';
import {Block} from './components';



// impoart all used images
const images = [
  require('./assets/icons/plants.png'),
  require('./assets/icons/back.png'),
  require('./assets/icons/pots.png'),
  require('./assets/icons/sprayers.png'),
  require('./assets/icons/flowers.png'),
  require('./assets/icons/seeds.png'),
  require('./assets/images/plants_1.png'),
  require('./assets/images/plants_2.png'),
  require('./assets/images/plants_3.png'),
  require('./assets/images/explore_1.png'),
  require('./assets/images/explore_2.png'),
  require('./assets/images/explore_3.png'),
  require('./assets/images/explore_4.png'),
  require('./assets/images/explore_5.png'),
  require('./assets/images/avatar.png'),

];



export default function App () {
  state = {
    isLoadingComplete : false,
  }
  handleResourcesAsync = async () => {
    // caching all images
    // for better performance on the app

    const cacheImages = images.map(img=> {
      return Asset.fromModule(image).downloadAsync();
    });

    return Promise.all(cacheImages);
  }

    if(!this.state.isLoadingComplete && this.props && !this.props.skipLoadingScreen){
      return (
        <AppLoading 
          startAsync={this.handleResourcesAsync}
          onError={error => console.warn(error)}
          onFinish={() => this.setState({isLoadingComplete: true})}
        />
      )
    } 
    return (
      <Block white >
        <Navigation />
      </Block>
    );

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
