import React, { Component } from "react";
import {
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  Alert
} from "react-native";

import { Button, Block, Input, Text } from "../components";
import { theme } from "../constants";

const VALID_EMAIL = "contact@esom-mail.com";
const VALID_PASSWORD = "contact@esom-mail.com";
export default class SignUp extends Component {
  state = {
    email: null,
    username: null,
    password: null,
    errors: [],
    loading: false
  };

  handleSignup() {
    const { navigation } = this.props;
    const { email, password, username } = this.state;
    const errors = [];

    Keyboard.dismiss();
    this.setState({ loading: true });

    // check with backend api or with some static data

    setTimeout(() => {
        if (!email) {
            errors.push("email");
          }
          if (!username) {
            errors.push("username");
          }
          if (!password) {
            errors.push("password");
          }
      
          this.setState({ errors, loading: false });
          if (!errors.length) {
            Alert.alert(
              "Success!",
              "Your account has been created",
              [
                {
                  text: "Continue",
                  onPress: () => {
                    navigation.navigate("Browse");
                  }
                }
              ],
              { cancelable: false }
            );
          }
    }, 2000)
  }

  render() {
    const { navigation } = this.props;
    const { loading, errors } = this.state;
    const hasErrors = key => (errors.includes(key) ? styles.hasErrors : null);

    return (
      <KeyboardAvoidingView style={styles.signup} behavior="padding">
        <Block padding={[0, theme.sizes.padding * 2]}>
          <Text h1 bold>
            {" "}
            Sign Up{" "}
          </Text>
          <Block middle>
            <Input
              label="Email"
              error={hasErrors("email")}
              style={[styles.input, hasErrors("email")]}
              defaultValue={this.state.email}
              onChangeText={text => this.setState({ email: text })}
            />

            <Input
              label="Username"
              error={hasErrors("username")}
              style={[styles.input, hasErrors("username")]}
              defaultValue={this.state.username}
              onChangeText={text => this.setState({ username: text })}
            />

            <Input
            
              label="Password"
              error={hasErrors("password")}
              style={[styles.input, hasErrors("password")]}
              defaultValue={this.state.password}
              onChangeText={text => this.setState({ password: text })}
            />

            <Button gradient onPress={() => this.handleSignup()}>
              {loading ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                <Text bold white center>
                  {" "}
                  Sign Up
                </Text>
              )}
            </Button>

            <Button onPress={() => navigation.goBack()}>
              <Text
                gray
                caption
                center
                style={{ textDecorationLine: "underline" }}
              >
                {" "}
                Back to login
              </Text>
            </Button>
          </Block>
        </Block>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  signup: {
    flex: 1,
    justifyContent: "center"
  },
  input: {
    borderRadius: 0,
    borderWidth: 0,
    borderBottomColor: theme.colors.gray2,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  hasErrors: {
    borderBottomColor: theme.colors.accent
  }
});
